# README #

Ubermont Readme

### Where do I find info and help? ###

You can find out help and bugs in the [Wiki](https://bitbucket.org/nolanblew/ubermont/wiki/Home).

### Where do I submit bug requests and tasks? ###

You can submit bug requests and tasks in the [Issues Page](https://bitbucket.org/nolanblew/ubermont/issues?status=new&status=open).

### How do I get set up? ###

* Details will be added soon!

### Contribution guidelines ###

* Make sure to have regular check-ins 