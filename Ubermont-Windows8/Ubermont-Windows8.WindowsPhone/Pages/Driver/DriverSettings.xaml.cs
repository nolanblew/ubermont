﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Ubermont_Windows8.Pages.Driver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DriverSettings : Page
    {

        DriverSettingsContext context = new DriverSettingsContext();

        public DriverSettings()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if ((string)e.Parameter == "first_time")
            {
                // Go to car settings
                pvtMain.SelectedIndex = 1;
            }

            GetSettings();
        }

        public async void GetSettings()
        {
            // Get the settings we need
            try {
                context.Car = await Service.UbermontService.Car.GetCar(Service.LocalService.loggedInUser.username);
            } catch
            {
                context.Car = new Entities.Car() { active = true, user = Service.LocalService.loggedInUser.username };
            }

            context.Driver = await Service.UbermontService.Driver.GetDriver(Service.LocalService.loggedInUser.username);
            context.User = Service.LocalService.loggedInUser;

            this.DataContext = context;

        }

        private void pvtMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pvtMain.SelectedIndex == 1)
                appBtnPhoto.Visibility = Visibility.Visible;
            else
                appBtnPhoto.Visibility = Visibility.Collapsed;
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // Save
            SaveSettings();
        }

        private async void SaveSettings()
        {
            await Service.UbermontService.Car.UpdateCar(context.Car);
            await Service.UbermontService.Driver.SetActiveUser(Service.LocalService.loggedInUser.username, context.Driver.active);
        }
    }

    public class DriverSettingsContext
    {

        public Entities.User User { get; set; }
        public Entities.Driver Driver { get; set; }
        public Entities.Car Car { get; set; }



    }
}
