﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Ubermont_Windows8.Pages.Driver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterDriver : Page
    {
        public RegisterDriver()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Check to see if the user is already a driver
            if (await Service.UbermontService.Driver.IsUserADriver(Service.LocalService.loggedInUser))
                // The user is already a driver, continue to the driver landing page
                Frame.Navigate(typeof(DriverMap));
            

        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // Accept the terms. Add this person as a driver
            try
            {
                await Service.UbermontService.Driver.AddUser(Service.LocalService.loggedInUser.username);

                await new MessageDialog("Congratulations! You are now a driver for Ubermont! Next, let's set up your car.", "Success").ShowAsync();
                Frame.Navigate(typeof(DriverSettings), "first_time");
            } catch (Exception ex)
            {
                await new MessageDialog("An error has occured! " + ex.Message + " Please try again.", "Oh no!").ShowAsync();
            }

        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            // The person denied the terms. Simply go back the the previous page they were on.
            Frame.GoBack();
        }
    }
}
