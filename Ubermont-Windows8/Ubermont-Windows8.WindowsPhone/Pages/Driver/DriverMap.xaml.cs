﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Ubermont_Windows8.Pages.Driver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DriverMap : Page
    {

        Geolocator gps = new Geolocator();

        public DriverMap()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            gps.DesiredAccuracy = PositionAccuracy.High;
            gps.MovementThreshold = 50;
            gps.DesiredAccuracyInMeters = 1;
            gps.StatusChanged += Gps_StatusChanged;
            gps.PositionChanged += Gps_PositionChanged;

        }

        public async void GetGPS()
        {
            var v = await gps.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(30));
            
        }


        private async void Gps_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (args.Status == PositionStatus.Ready)
                    grdGPSError.Visibility = Visibility.Collapsed;
                else
                {
                    if (args.Status == PositionStatus.Disabled)
                        lblGPSError.Text = "GPS Is disabled";
                    else if (args.Status == PositionStatus.Initializing)
                        lblGPSError.Text = "Searching...";
                    else if (args.Status == PositionStatus.NoData)
                        lblGPSError.Text = "Cannot find GPS";
                    else
                        lblGPSError.Text = "GPS Error";

                    grdGPSError.Visibility = Visibility.Visible;
                }
            });
        }

        private async void Gps_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                map.Center = args.Position.Coordinate.Point; //new Geopoint(new BasicGeoposition() { Latitude = args.Position.Coordinate.Latitude, Longitude = args.Position.Coordinate.Latitude, Altitude = args.Position.Coordinate.Altitude.Value });
                map.ZoomLevel = 18;


                map.MapElements.Clear();

                MapIcon icon = new MapIcon();
                icon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/me.png"));
                icon.Title = "Current Location";
                icon.Location = args.Position.Coordinate.Point;
                icon.NormalizedAnchorPoint = new Point(0.5, 0.5);
                
                map.MapElements.Add(icon);

                try
                {
                    Service.UbermontService.Driver.UpdatePosition(Service.LocalService.loggedInUser.username, args.Position.Coordinate.Point);
                } catch { }

            });
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // Switch to Rider
            Service.UbermontService.Driver.SetActiveUser(Service.LocalService.loggedInUser.username, false);
            Frame.Navigate(typeof(Rider.FindRides));
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            // Driver Settings
            Frame.Navigate(typeof(DriverSettings));
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            // Logout
            Service.UbermontService.Driver.SetActiveUser(Service.LocalService.loggedInUser.username, false);
            Service.LocalService.Logout();
            Frame.Navigate(typeof(Login));
        }
    }
}
