﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Ubermont_Windows8.Pages.Rider
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DriverDetail : Page
    {
        public DriverDetail()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (Service.LocalService.selectedDriver == null)
            {
                // An error occured, go back
                await new MessageDialog("Oh no! An error has occured. Please try again.", "Error!").ShowAsync();
                Frame.GoBack();
            }

            // Load up the car
            LoadDriverPage();
        }

        async void LoadDriverPage()
        {
            RiderContext context = new RiderContext(Service.LocalService.selectedDriver);

            context.current_car = new Entities.Car()
            {
                active = true,
                id = 1,
                manufacturer = "Mazda",
                model = "3",
                year = "2007",
                photo = "http://www.todoautos.com.pe/attachments/f41/315580d1271950061-mazda3-sport-2-0-mecanico-nacional-2007-dscn3999-f2.jpg"
            };

            context.minutes_away = 12;

            this.DataContext = context;

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // Activate the car and procede to the map

            Frame.Navigate(typeof(RiderMap));
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            /// TODO: Add code for messaging
        }
    }

    public class RiderContext : Entities.Driver
    {

        public RiderContext() { }

        public RiderContext(Entities.Driver driver)
        {
            this.active = driver.active;
            this.current_position = driver.current_position;
            this.end_position = driver.end_position;
            this.id = driver.id;
            this.start_position = driver.start_position;
            this.user = driver.user;
            this.User1 = driver.User1;
        }

        private Entities.Car _current_car;

        public Entities.Car current_car
        {
            get { return _current_car; }
            set
            {
                if (_current_car != value)
                {
                    _current_car = value;
                    OnPropertyChanged("current_car");
                }
            }
        }


        private int _minutes_away;
        /// <summary>
        /// How many minutes away the driver is
        /// </summary>
        public int minutes_away
        {
            get { return _minutes_away; }
            set
            {
                if (_minutes_away != value)
                {
                    _minutes_away = value;
                    OnPropertyChanged("minutes_away");
                }
            }
        }


    }
}
