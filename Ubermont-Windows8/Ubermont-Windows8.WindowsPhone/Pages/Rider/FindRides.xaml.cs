﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Services.Maps;
using Windows.Devices.Geolocation;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Ubermont_Windows8.Pages.Rider
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FindRides : Page
    {
        public FindRides()
        {
            this.InitializeComponent();
            MapService.ServiceToken = "AtoOG69yaNX2A4aNlp3cR1WNmYmkMCAo_ghNOtdNfq767nWS-dOu1I7f9x8qBh8-";
        }

        async Task<List<MapLocation>> GetAddresses(string search, Geopoint location)
        {
            MapLocationFinderResult result = await MapLocationFinder.FindLocationsAsync(search, location, 5);
            
            if (result.Status == MapLocationFinderStatus.Success)
                return result.Locations.ToList();

            return null;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {

            lblNoResults.Visibility = Visibility.Collapsed;
            prgRing.Visibility = Visibility.Visible;
            prgRing.IsActive = true;

            // Set fake data context for testing
            List<Entities.Driver> drivers = await Service.UbermontService.Driver.FindActiveDrivers();

            lstResults.ItemsSource = drivers;

            prgRing.IsActive = false;
            if (drivers.Count == 0)
                lblNoResults.Visibility = Visibility.Visible;
        }

        private void lstResults_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lstResults.SelectedItem == null) return;

            // Go to the driver page
            Service.LocalService.selectedDriver = (Entities.Driver)lstResults.SelectedItem;
            Frame.Navigate(typeof(DriverDetail));

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // User is requesting to go to driver mode. Take them to the driver landing page
            Frame.Navigate(typeof(Pages.Driver.RegisterDriver));
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            // Go to the settings page
            /// TODO: Add settings page
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            // Logout the user
            Service.LocalService.selectedDriver = null;
            Service.LocalService.loggedInUser = null;

            Frame.Navigate(typeof(Login));
        }
    }
}
