﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.PushNotifications;
using Microsoft.WindowsAzure.Messaging;

namespace Ubermont_Windows8
{
    public static class PushHelper
    {

        public static async void RegisterPushChannel()
        {
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
            var hub = new NotificationHub("ubermonthub", "Endpoint=sb://ubermonthub-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=qFzeLdJkgtFBXbpOMiG7aVj3ndQ1xjgsDJD1hRqYyFw=");
            var tag = new List<string>();
            tag.Add(Service.LocalService.loggedInUser.username);
            var result = await hub.RegisterNativeAsync(channel.Uri, tag);

            // Send the notification channel to the server
            await Service.UbermontService.User.UpdatePush(Service.LocalService.loggedInUser.username, result.ChannelUri);
            
        }



    }
}
