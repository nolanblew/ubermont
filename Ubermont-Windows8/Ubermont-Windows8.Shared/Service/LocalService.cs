﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ubermont_Windows8.Service
{
    public static class LocalService
    {
        public static Entities.User loggedInUser { get; set; }


        #region Temporary Variables

        public static Entities.Driver selectedDriver { get; set; }

        #endregion

        public static void Logout()
        {
            selectedDriver = null;
            loggedInUser = null;
        }

    }
}
