﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Windows.Web;
using Newtonsoft.Json;
using Windows.Devices.Geolocation;

namespace Ubermont_Windows8.Service
{
    public static class UbermontService
    {
        static readonly string BaseURL = "http://ubermont-api.azurewebsites.net/api/dev/";


        public static class User
        {
            /// <summary>
            /// Checks if the user's username and password is correct
            /// </summary>
            /// <param name="username">The username to check</param>
            /// <param name="password">The password to check</param>
            /// <returns>True if the credentials are correct, false if they are not</returns>
            public static async Task<bool> CheckLogin(string username, string password)
            {
                string codedJSON = await GetJsonAPI("User", "login", new KeyValuePair<string, string>("username", username),
                                                                     new KeyValuePair<string, string>("password", password));
                var definition = new { auth = false };
                var rtn = JsonConvert.DeserializeAnonymousType(codedJSON, definition);

                return rtn.auth;
            }

            /// <summary>
            /// Gets a user object based on the username
            /// </summary>
            /// <param name="username">The username of the user</param>
            /// <returns>The User object returned by the database (does not include the password)</returns>
            public static async Task<Entities.User> GetUser(string username)
            {
                string codedJSON = await GetJsonAPI("User", "get_user", new KeyValuePair<string, string>("username", username));

                return JsonConvert.DeserializeObject<Entities.User>(codedJSON);
            }

            /// <summary>
            /// Register the user given a User object
            /// </summary>
            /// <param name="user">The User with username, password, and other attributes</param>
            public static async Task RegisterUser(Entities.User user)
            {
                string codedJson = await GetJsonAPI("User", "register", new KeyValuePair<string, string>("username", user.username),
                                                                        new KeyValuePair<string, string>("password", user.password),
                                                                        new KeyValuePair<string, string>("full_name", user.full_name));

                Result r = JsonConvert.DeserializeObject<Result>(codedJson);

                if (!r)
                    throw new Exception(r.Message);
            }

            /// <summary>
            /// Updates a user's Push notifications
            /// </summary>
            /// <param name="username">The username of the user</param>
            /// <param name="pushChannel">the push channel of the user</param>
            public static async Task UpdatePush(string username, string pushChannel)
            {
                string codedJson = await GetJsonAPI("User", "update_push", new KeyValuePair<string, string>("username", username),
                                                                           new KeyValuePair<string, string>("push_channel", pushChannel));

                Result r = JsonConvert.DeserializeObject<Result>(codedJson);

                if (!r)
                    throw new Exception(r.Message);
            }
        }

        public static class Driver
        {

            /// <summary>
            /// Adds a user to the driver's list
            /// </summary>
            /// <param name="user_id">The ID of the user</param>
            /// <exception cref="Result">A message and stacktrace if the server is unable to create a driver</exception>
            public static async Task AddUser(string user_id)
            {
                string codedJSON = await GetJsonAPI("Driver", "new_driver", new KeyValuePair<string, string>("id", user_id));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (r.Status != "Success")
                    throw new Exception(r.Message);

            }

            /// <summary>
            /// Sets the driver as either active or inactive
            /// </summary>
            /// <param name="user_id">The ID of the user on which to set their driver status</param>
            /// <param name="is_active">The status of the driver - True is active False is inactive</param>
            /// <exception cref="Result">A message and stacktrace if the server is unable to set the driver status</exception>
            public static async Task SetActiveUser(string user_id, bool is_active)
            {
                string codedJSON = await GetJsonAPI("Driver", "set_driver_active", new KeyValuePair<string, string>("id", user_id),
                                                                                   new KeyValuePair<string, string>("is_active", is_active.ToString()));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (r.Status != "Success")
                    throw new Exception(r.Message);
            }

            /// <summary>
            /// Will get all active drivers in range
            /// </summary>
            public static async Task<List<Entities.Driver>> FindActiveDrivers()
            {
                string codedJSON = await GetJsonAPI("Driver", "find_active_drivers");

                try
                {

                    List<Entities.Driver> rtn = JsonConvert.DeserializeObject<List<Entities.Driver>>(codedJSON, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                    return rtn;

                }
                catch (Exception ex)
                {
                    throw;
                }

            }

            /// <summary>
            /// Checks to see if the user is a driver
            /// </summary>
            /// <param name="user">The user to check to see if they are a driver</param>
            /// <returns>True if the user is a driver, false otherwise</returns>
            public static async Task<bool> IsUserADriver(Entities.User user)
            {
                string codedJSON = await GetJsonAPI("Driver", "is_user_a_driver", new KeyValuePair<string, string>("id", user.username));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (r)
                {
                    return r.Return<bool>();
                }
                else
                {
                    throw new Exception(r.Message);
                }

            }

            /// <summary>
            /// Gets the entire Driver class from the database from the username
            /// </summary>
            /// <param name="username">The username of the driver</param>
            /// <returns>The driver class of that user</returns>
            public static async Task<Entities.Driver> GetDriver(string username)
            {
                string codedJSON = await GetJsonAPI("Driver", "get_driver", new KeyValuePair<string, string>("id", username));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

                if (r)
                {
                    return r.Return<Entities.Driver>();
                }
                else
                {
                    throw new Exception(r.Message);
                }

            }

            /// <summary>
            /// Updates the position of a driver
            /// </summary>
            /// <param name="username">The username of the driver</param>
            /// <param name="position">The current position of the driver</param>
            public static async Task UpdatePosition(string username, Geopoint position)
            {
                string codedJSON = await GetJsonAPI("Driver", "update_position", new KeyValuePair<string, string>("id", username),
                                                                                 new KeyValuePair<string, string>("position", JsonConvert.SerializeObject(position)));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (!r)
                    throw new Exception(r.Message);

            }

            /// <summary>
            /// Starts the route of the driver
            /// </summary>
            /// <param name="username">The username of the driver</param>
            /// <param name="start_position">The start point of the driver</param>
            /// <param name="end_position">The end point of the driver</param>
            /// <param name="current_position">The current position of the driver</param>
            /// <returns></returns>
            public static async Task StartRoute(string username, string start_position, string end_position, string current_position)
            {
                string codedJSON = await GetJsonAPI("Driver", "start_route", new KeyValuePair<string, string>("id", username),
                                                                             new KeyValuePair<string, string>("spos", start_position),
                                                                             new KeyValuePair<string, string>("epos", end_position),
                                                                             new KeyValuePair<string, string>("cpos", current_position));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (!r)
                    throw new Exception(r.Message);


            }

        }

        public static class Car
        {
            /// <summary>
            /// Updates a car, or adds one if the car does not exist
            /// </summary>
            /// <param name="car">The car class</param>
            public static async Task UpdateCar(Entities.Car car)
            {
                string codedJSON = await GetJsonAPI("Car", "update_car", new KeyValuePair<string, string>("username", car.user),
                                                                         new KeyValuePair<string, string>("make", car.manufacturer),
                                                                         new KeyValuePair<string, string>("model", car.model),
                                                                         new KeyValuePair<string, string>("year", car.year),
                                                                         new KeyValuePair<string, string>("photo", car.photo));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (!r)
                    throw new Exception(r.Message);

            }

            /// <summary>
            /// Sets a user's car as active
            /// </summary>
            /// <param name="username">The username who owns the car</param>
            /// <param name="isActive">Weather or not the car is active or not</param>
            public static async Task SetCarActive(string username, bool isActive)
            {
                string codedJSON = await GetJsonAPI("Car", "set_car_active", new KeyValuePair<string, string>("username", username),
                                                                             new KeyValuePair<string, string>("is_active", isActive.ToString()));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (!r)
                    throw new Exception(r.Message);
            }

            /// <summary>
            /// Gets a car object from a username
            /// </summary>
            /// <param name="username">The username of the person who owns the car</param>
            /// <returns>A car object</returns>
            public static async Task<Entities.Car> GetCar(string username)
            {
                string codedJSON = await GetJsonAPI("Car", "get_drivers_car", new KeyValuePair<string, string>("username", username));

                Result r = JsonConvert.DeserializeObject<Result>(codedJSON);

                if (!r)
                    throw new Exception(r.Message);

                return r.Return<Entities.Car>();
            }
        }



        static async Task<string> GetJsonAPI(string page, string action, params KeyValuePair<string, string>[] parms)
        {
            string url = BaseURL + page + ".ashx?rnd=10&action=" + action;
            foreach (KeyValuePair<string, string> key in parms)
                url += string.Format("&{0}={1}", key.Key, key.Value);

            Windows.Web.Http.HttpClient client = new Windows.Web.Http.HttpClient();

            return await client.GetStringAsync(new Uri(url));
        }
    }

    public class Result
    {
        public string return_encoded;

        public void Return(object rtn)
        {
            return_encoded = JsonConvert.SerializeObject(rtn, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public T Return<T>()
        {
            return JsonConvert.DeserializeObject<T>(return_encoded, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }


        public string Status { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public static implicit operator bool (Result r)
        {
            return r.Status == "Success";
        }
    }
}
