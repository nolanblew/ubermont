﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Ubermont_Windows8.Entities
{
    public class Driver : INotifyPropertyChanged
    {

        #region Constructor

        public Driver() {
            _id = -1;
        }

        #endregion


        #region Properties

        private int _id;
        /// <summary>
        /// The driver ID as set in the database
        /// </summary>
        public int id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("id");
                }
            }
        }


        private string _userID;
        /// <summary>
        /// The ID of the user
        /// </summary>
        public string user
        {
            get { return _userID; }
            set
            {
                if (_userID != value)
                {
                    _userID = value;
                    OnPropertyChanged("user");
                }
            }
        }


        private User _user1;
        /// <summary>
        /// The user object retrieved from the database
        /// </summary>
        public User User1
        {
            get { return _user1; }
            set
            {
                if (_user1 != value)
                {
                    _user1 = value;
                    OnPropertyChanged("User1");
                }
            }
        }
        

        private bool _active;
        /// <summary>
        /// If the driver is currently actively picking up passengers
        /// </summary>
        public bool active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;
                    OnPropertyChanged("active");
                }
            }
        }


        private string _current_position;
        /// <summary>
        /// The string coordinates of the current position of the driver
        /// </summary>
        public string current_position
        {
            get { return _current_position; }
            set
            {
                if (_current_position != value)
                {
                    _current_position = value;
                    OnPropertyChanged("current_position");
                }
            }
        }


        private string _start_position;
        /// <summary>
        /// The string coordinates of the start position of the driver
        /// </summary>
        public string start_position
        {
            get { return _start_position; }
            set
            {
                if (_start_position != value)
                {
                    _start_position = value;
                    OnPropertyChanged("start_position");
                }
            }
        }


        private string _end_position;
        /// <summary>
        /// The string coordinates of the destination of the driver
        /// </summary>
        public string end_position
        {
            get { return _end_position; }
            set
            {
                if (_end_position != value)
                {
                    _end_position = value;
                    OnPropertyChanged("end_position");
                }
            }
        }



        #endregion


        #region Property Change Events

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}
