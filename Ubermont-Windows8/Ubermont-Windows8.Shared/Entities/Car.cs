﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Ubermont_Windows8.Entities
{
    public class Car : INotifyPropertyChanged
    {

        #region Constructor

        /// <summary>
        /// Creates a new Car class
        /// </summary>
        public Car() { }

        #endregion

        #region Properties

        private int _id;
        /// <summary>
        /// The database-assigned ID of the car
        /// </summary>
        public int id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("id");
                }
            }
        }


        private string _user_id;
        /// <summary>
        /// The ID of the user that the car belongs to
        /// </summary>
        public string user
        {
            get { return _user_id; }
            set
            {
                if (_user_id != value)
                {
                    _user_id = value;
                    OnPropertyChanged("user");
                }
            }
        }


        private bool _active;
        /// <summary>
        /// Determines if the car is the active car used by the driver
        /// </summary>
        public bool active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;
                    OnPropertyChanged("active");
                }
            }
        }


        private string _manufacturer;
        /// <summary>
        /// The manufacturer of the car
        /// </summary>
        public string manufacturer
        {
            get { return _manufacturer; }
            set
            {
                if (_manufacturer != value)
                {
                    _manufacturer = value;
                    OnPropertyChanged("manufacturer");
                }
            }
        }


        private string _model;
        /// <summary>
        /// The model of the car
        /// </summary>
        public string model
        {
            get { return _model; }
            set
            {
                if (_model != value)
                {
                    _model = value;
                    OnPropertyChanged("model");
                }
            }
        }


        private string _year;
        /// <summary>
        /// A string-representation of the year of the car. Max length is 5
        /// </summary>
        public string year
        {
            get { return _year; }
            set
            {
                if (_year != value)
                {
                    _year = value;
                    OnPropertyChanged("year");
                }
            }
        }


        private string _photo;
        /// <summary>
        /// The string URL of the location of the photo of the car
        /// </summary>
        public string photo
        {
            get { return _photo; }
            set
            {
                if (_photo != value)
                {
                    _photo = value;
                    OnPropertyChanged("photo");
                }
            }
        }



        #endregion

        #region Property Change Events

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

    }
}
