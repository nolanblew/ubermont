﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Ubermont_Windows8.Entities
{
    public class User : INotifyPropertyChanged
    {

        #region Constructors

        /// <summary>
        /// Creates a blank User object
        /// </summary>
        public User() { }

        /// <summary>
        /// Creates a new User object
        /// </summary>
        /// <param name="username">The username (ID) of the User</param>
        public User(string username) { this.username = username; }

        #endregion

        #region Properties

        private string _username;
        /// <summary>
        /// User's Username
        /// </summary>
        public string username
        {
            get { return _username; }
            set
            {
                if (_username != value)
                {
                    _username = value;
                    propertyChanged("username");
                }
            }
        }

        private string _password;
        /// <summary>
        /// User's Password
        /// </summary>
        public string password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    propertyChanged("password");
                }
            }
        }

        private string _fullName;
        /// <summary>
        /// The Full Name of the person
        /// </summary>
        public string full_name
        {
            get { return _fullName; }
            set
            {
                if (_fullName != value)
                {
                    _fullName = value;
                    propertyChanged("full_name");
                }
            }
        }


        private DateTime? _dateCreated;
        /// <summary>
        /// The date the user was created/registered (do not change)
        /// </summary>
        public DateTime? date_created
        {
            get { return _dateCreated; }
            set
            {
                if (_dateCreated != value)
                {
                    _dateCreated = value;
                    propertyChanged("date_created");
                }
            }
        }


        private DateTime? _lastAccessed;
        /// <summary>
        /// The date the user last accessed his/her account
        /// </summary>
        public DateTime? last_accessed
        {
            get { return _lastAccessed; }
            set
            {
                if (_lastAccessed != value)
                {
                    _lastAccessed = value;
                    propertyChanged("last_accessed");
                }
            }
        }


        private int _devices;
        /// <summary>
        /// The number of devices the user has used their account on
        /// </summary>
        public int devices
        {
            get { return _devices; }
            set
            {
                if (_devices != value)
                {
                    _devices = value;
                    propertyChanged("devices");
                }
            }
        }


        private string _push_channel;

        public string push_channel
        {
            get { return _push_channel; }
            set
            {
                if (_push_channel != value)
                {
                    _push_channel = value;
                    propertyChanged("push_channel");
                }
            }
        }


        #endregion


        #region Property Change Events

        public event PropertyChangedEventHandler PropertyChanged;

        public void propertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}
