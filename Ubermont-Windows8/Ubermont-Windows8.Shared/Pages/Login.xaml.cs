using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Ubermont_Windows8.Entities;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Ubermont_Windows8
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        public Login()
        {
            this.InitializeComponent();
            this.Loaded += Login_Loaded;
        }

        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey("uslama"))
                txtUsername.Text = (string)localSettings.Values["uslama"];

        }

        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            prgRing.IsActive = true;
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;
            btnLogin.IsEnabled = false;
            
            // Try to login
            if (await Service.UbermontService.User.CheckLogin(txtUsername.Text, txtPassword.Password))
            {
                // Login successful. Get their username
                User usr = await Service.UbermontService.User.GetUser(txtUsername.Text);
                Service.LocalService.loggedInUser = usr;

                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values["uslama"] = txtUsername.Text;
                localSettings.Values["ajsea"] = txtPassword.Password;

                Frame.Navigate(typeof(Pages.Rider.FindRides));

            } else
            {
                // Login not successful
                MessageDialog md = new MessageDialog("Incorrect username or password!", "Login");
                await md.ShowAsync();

                txtPassword.Password = "";
            }

            txtUsername.IsEnabled = true;
            txtPassword.IsEnabled = true;
            prgRing.IsActive = false;
            btnLogin.IsEnabled = true;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            // Go to the registration page
            Frame.Navigate(typeof(Pages.Register));
        }
    }
}
