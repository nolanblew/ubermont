﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Ubermont_Windows8.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Register : Page
    {
        public Register()
        {
            this.InitializeComponent();
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // Attempt to register the user
            txtEmail.IsEnabled = false;
            txtUsername.IsEnabled = false;
            txtPassword.IsEnabled = false;
            txtFullName.IsEnabled = false;

            BottomAppBar.IsEnabled = false;

            Entities.User usr = new Entities.User()
            {
                username = txtUsername.Text,
                password = txtPassword.Password,
                full_name = txtFullName.Text,
                devices = 1
            };

            try {
                await Service.UbermontService.User.RegisterUser(usr);


                //Bring the user back to the login page if they are successful
                /// TODO: Change this to logging in the user when they are successful.

                await new MessageDialog("You have registered. Please log in to continue!").ShowAsync();
                Frame.GoBack();

            } catch (Exception ex)
            {
                await new MessageDialog("Error! " + ex.Message).ShowAsync();
                txtEmail.IsEnabled = true;
                txtUsername.IsEnabled = true;
                txtPassword.IsEnabled = true;
                txtFullName.IsEnabled = true;
                BottomAppBar.IsEnabled = true;
            }

        }

        private void txtEmail_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            // Update the text to include the '@westmont.edu' address

            var cursor = txtEmail.SelectionStart;
            var selection_length = txtEmail.SelectionLength;

            // Check to see if there is already an @ in the text, then remove it
            if (txtEmail.Text.Contains("@"))
                txtEmail.Text = txtEmail.Text.Substring(0, txtEmail.Text.IndexOf('@'));

            // Add the westmont edu at the end
            if (txtEmail.Text.Length > 0)
                txtEmail.Text = txtEmail.Text + "@westmont.edu";

            // Reset the cursor
            txtEmail.Select(cursor, selection_length);
        }
    }
}
