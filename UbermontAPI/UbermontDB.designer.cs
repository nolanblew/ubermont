﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.0
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UbermontAPI
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Ubermont_db")]
	public partial class UbermontDBDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertAccessKey(AccessKey instance);
    partial void UpdateAccessKey(AccessKey instance);
    partial void DeleteAccessKey(AccessKey instance);
    partial void Insertdriver(driver instance);
    partial void Updatedriver(driver instance);
    partial void Deletedriver(driver instance);
    partial void Insertcar(car instance);
    partial void Updatecar(car instance);
    partial void Deletecar(car instance);
    partial void Insertuser(user instance);
    partial void Updateuser(user instance);
    partial void Deleteuser(user instance);
    #endregion
		
		public UbermontDBDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["Ubermont_dbConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public UbermontDBDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UbermontDBDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UbermontDBDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UbermontDBDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<AccessKey> AccessKeys
		{
			get
			{
				return this.GetTable<AccessKey>();
			}
		}
		
		public System.Data.Linq.Table<driver> drivers
		{
			get
			{
				return this.GetTable<driver>();
			}
		}
		
		public System.Data.Linq.Table<car> cars
		{
			get
			{
				return this.GetTable<car>();
			}
		}
		
		public System.Data.Linq.Table<user> users
		{
			get
			{
				return this.GetTable<user>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.access_keys")]
	public partial class AccessKey : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _key;
		
		private string _name;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnkeyChanging(string value);
    partial void OnkeyChanged();
    partial void OnnameChanging(string value);
    partial void OnnameChanged();
    #endregion
		
		public AccessKey()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[key]", Storage="_key", DbType="VarChar(MAX) NOT NULL", CanBeNull=false)]
		public string key
		{
			get
			{
				return this._key;
			}
			set
			{
				if ((this._key != value))
				{
					this.OnkeyChanging(value);
					this.SendPropertyChanging();
					this._key = value;
					this.SendPropertyChanged("key");
					this.OnkeyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_name", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string name
		{
			get
			{
				return this._name;
			}
			set
			{
				if ((this._name != value))
				{
					this.OnnameChanging(value);
					this.SendPropertyChanging();
					this._name = value;
					this.SendPropertyChanged("name");
					this.OnnameChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.drivers")]
	public partial class driver : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _user;
		
		private System.Nullable<bool> _active;
		
		private string _current_position;
		
		private string _start_position;
		
		private string _end_position;
		
		private EntityRef<user> _user1;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnuserChanging(string value);
    partial void OnuserChanged();
    partial void OnactiveChanging(System.Nullable<bool> value);
    partial void OnactiveChanged();
    partial void Oncurrent_positionChanging(string value);
    partial void Oncurrent_positionChanged();
    partial void Onstart_positionChanging(string value);
    partial void Onstart_positionChanged();
    partial void Onend_positionChanging(string value);
    partial void Onend_positionChanged();
    #endregion
		
		public driver()
		{
			this._user1 = default(EntityRef<user>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[user]", Storage="_user", DbType="VarChar(30) NOT NULL", CanBeNull=false)]
		public string user
		{
			get
			{
				return this._user;
			}
			set
			{
				if ((this._user != value))
				{
					this.OnuserChanging(value);
					this.SendPropertyChanging();
					this._user = value;
					this.SendPropertyChanged("user");
					this.OnuserChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_active", DbType="Bit")]
		public System.Nullable<bool> active
		{
			get
			{
				return this._active;
			}
			set
			{
				if ((this._active != value))
				{
					this.OnactiveChanging(value);
					this.SendPropertyChanging();
					this._active = value;
					this.SendPropertyChanged("active");
					this.OnactiveChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_current_position", DbType="VarChar(150)")]
		public string current_position
		{
			get
			{
				return this._current_position;
			}
			set
			{
				if ((this._current_position != value))
				{
					this.Oncurrent_positionChanging(value);
					this.SendPropertyChanging();
					this._current_position = value;
					this.SendPropertyChanged("current_position");
					this.Oncurrent_positionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_start_position", DbType="VarChar(150)")]
		public string start_position
		{
			get
			{
				return this._start_position;
			}
			set
			{
				if ((this._start_position != value))
				{
					this.Onstart_positionChanging(value);
					this.SendPropertyChanging();
					this._start_position = value;
					this.SendPropertyChanged("start_position");
					this.Onstart_positionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_end_position", DbType="VarChar(150)")]
		public string end_position
		{
			get
			{
				return this._end_position;
			}
			set
			{
				if ((this._end_position != value))
				{
					this.Onend_positionChanging(value);
					this.SendPropertyChanging();
					this._end_position = value;
					this.SendPropertyChanged("end_position");
					this.Onend_positionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="user_driver", Storage="_user1", ThisKey="user", OtherKey="username", IsForeignKey=true)]
		public user user1
		{
			get
			{
				return this._user1.Entity;
			}
			set
			{
				user previousValue = this._user1.Entity;
				if (((previousValue != value) 
							|| (this._user1.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._user1.Entity = null;
						previousValue.drivers.Remove(this);
					}
					this._user1.Entity = value;
					if ((value != null))
					{
						value.drivers.Add(this);
						this._user = value.username;
					}
					else
					{
						this._user = default(string);
					}
					this.SendPropertyChanged("user1");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.cars")]
	public partial class car : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _user;
		
		private System.Nullable<bool> _active;
		
		private string _manufacturer;
		
		private string _model;
		
		private string _year;
		
		private string _photo;
		
		private EntityRef<user> _user1;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnuserChanging(string value);
    partial void OnuserChanged();
    partial void OnactiveChanging(System.Nullable<bool> value);
    partial void OnactiveChanged();
    partial void OnmanufacturerChanging(string value);
    partial void OnmanufacturerChanged();
    partial void OnmodelChanging(string value);
    partial void OnmodelChanged();
    partial void OnyearChanging(string value);
    partial void OnyearChanged();
    partial void OnphotoChanging(string value);
    partial void OnphotoChanged();
    #endregion
		
		public car()
		{
			this._user1 = default(EntityRef<user>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[user]", Storage="_user", DbType="VarChar(30) NOT NULL", CanBeNull=false)]
		public string user
		{
			get
			{
				return this._user;
			}
			set
			{
				if ((this._user != value))
				{
					this.OnuserChanging(value);
					this.SendPropertyChanging();
					this._user = value;
					this.SendPropertyChanged("user");
					this.OnuserChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_active", DbType="Bit")]
		public System.Nullable<bool> active
		{
			get
			{
				return this._active;
			}
			set
			{
				if ((this._active != value))
				{
					this.OnactiveChanging(value);
					this.SendPropertyChanging();
					this._active = value;
					this.SendPropertyChanged("active");
					this.OnactiveChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_manufacturer", DbType="VarChar(30)")]
		public string manufacturer
		{
			get
			{
				return this._manufacturer;
			}
			set
			{
				if ((this._manufacturer != value))
				{
					this.OnmanufacturerChanging(value);
					this.SendPropertyChanging();
					this._manufacturer = value;
					this.SendPropertyChanged("manufacturer");
					this.OnmanufacturerChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_model", DbType="VarChar(30)")]
		public string model
		{
			get
			{
				return this._model;
			}
			set
			{
				if ((this._model != value))
				{
					this.OnmodelChanging(value);
					this.SendPropertyChanging();
					this._model = value;
					this.SendPropertyChanged("model");
					this.OnmodelChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_year", DbType="VarChar(5)")]
		public string year
		{
			get
			{
				return this._year;
			}
			set
			{
				if ((this._year != value))
				{
					this.OnyearChanging(value);
					this.SendPropertyChanging();
					this._year = value;
					this.SendPropertyChanged("year");
					this.OnyearChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_photo", DbType="VarChar(100)")]
		public string photo
		{
			get
			{
				return this._photo;
			}
			set
			{
				if ((this._photo != value))
				{
					this.OnphotoChanging(value);
					this.SendPropertyChanging();
					this._photo = value;
					this.SendPropertyChanged("photo");
					this.OnphotoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="user_car", Storage="_user1", ThisKey="user", OtherKey="username", IsForeignKey=true)]
		public user user1
		{
			get
			{
				return this._user1.Entity;
			}
			set
			{
				user previousValue = this._user1.Entity;
				if (((previousValue != value) 
							|| (this._user1.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._user1.Entity = null;
						previousValue.cars.Remove(this);
					}
					this._user1.Entity = value;
					if ((value != null))
					{
						value.cars.Add(this);
						this._user = value.username;
					}
					else
					{
						this._user = default(string);
					}
					this.SendPropertyChanged("user1");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.users")]
	public partial class user : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _username;
		
		private string _password;
		
		private string _full_name;
		
		private System.Nullable<System.DateTime> _date_created;
		
		private System.Nullable<System.DateTime> _last_accesed;
		
		private int _devices;
		
		private string _push_channel;
		
		private EntitySet<driver> _drivers;
		
		private EntitySet<car> _cars;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnusernameChanging(string value);
    partial void OnusernameChanged();
    partial void OnpasswordChanging(string value);
    partial void OnpasswordChanged();
    partial void Onfull_nameChanging(string value);
    partial void Onfull_nameChanged();
    partial void Ondate_createdChanging(System.Nullable<System.DateTime> value);
    partial void Ondate_createdChanged();
    partial void Onlast_accesedChanging(System.Nullable<System.DateTime> value);
    partial void Onlast_accesedChanged();
    partial void OndevicesChanging(int value);
    partial void OndevicesChanged();
    partial void Onpush_channelChanging(string value);
    partial void Onpush_channelChanged();
    #endregion
		
		public user()
		{
			this._drivers = new EntitySet<driver>(new Action<driver>(this.attach_drivers), new Action<driver>(this.detach_drivers));
			this._cars = new EntitySet<car>(new Action<car>(this.attach_cars), new Action<car>(this.detach_cars));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_username", DbType="VarChar(30) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string username
		{
			get
			{
				return this._username;
			}
			set
			{
				if ((this._username != value))
				{
					this.OnusernameChanging(value);
					this.SendPropertyChanging();
					this._username = value;
					this.SendPropertyChanged("username");
					this.OnusernameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_password", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string password
		{
			get
			{
				return this._password;
			}
			set
			{
				if ((this._password != value))
				{
					this.OnpasswordChanging(value);
					this.SendPropertyChanging();
					this._password = value;
					this.SendPropertyChanged("password");
					this.OnpasswordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_full_name", DbType="VarChar(150)")]
		public string full_name
		{
			get
			{
				return this._full_name;
			}
			set
			{
				if ((this._full_name != value))
				{
					this.Onfull_nameChanging(value);
					this.SendPropertyChanging();
					this._full_name = value;
					this.SendPropertyChanged("full_name");
					this.Onfull_nameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_date_created", DbType="DateTime")]
		public System.Nullable<System.DateTime> date_created
		{
			get
			{
				return this._date_created;
			}
			set
			{
				if ((this._date_created != value))
				{
					this.Ondate_createdChanging(value);
					this.SendPropertyChanging();
					this._date_created = value;
					this.SendPropertyChanged("date_created");
					this.Ondate_createdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_last_accesed", DbType="DateTime")]
		public System.Nullable<System.DateTime> last_accesed
		{
			get
			{
				return this._last_accesed;
			}
			set
			{
				if ((this._last_accesed != value))
				{
					this.Onlast_accesedChanging(value);
					this.SendPropertyChanging();
					this._last_accesed = value;
					this.SendPropertyChanged("last_accesed");
					this.Onlast_accesedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_devices", DbType="Int NOT NULL")]
		public int devices
		{
			get
			{
				return this._devices;
			}
			set
			{
				if ((this._devices != value))
				{
					this.OndevicesChanging(value);
					this.SendPropertyChanging();
					this._devices = value;
					this.SendPropertyChanged("devices");
					this.OndevicesChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_push_channel", DbType="VarChar(MAX)")]
		public string push_channel
		{
			get
			{
				return this._push_channel;
			}
			set
			{
				if ((this._push_channel != value))
				{
					this.Onpush_channelChanging(value);
					this.SendPropertyChanging();
					this._push_channel = value;
					this.SendPropertyChanged("push_channel");
					this.Onpush_channelChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="user_driver", Storage="_drivers", ThisKey="username", OtherKey="user")]
		public EntitySet<driver> drivers
		{
			get
			{
				return this._drivers;
			}
			set
			{
				this._drivers.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="user_car", Storage="_cars", ThisKey="username", OtherKey="user")]
		public EntitySet<car> cars
		{
			get
			{
				return this._cars;
			}
			set
			{
				this._cars.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_drivers(driver entity)
		{
			this.SendPropertyChanging();
			entity.user1 = this;
		}
		
		private void detach_drivers(driver entity)
		{
			this.SendPropertyChanging();
			entity.user1 = null;
		}
		
		private void attach_cars(car entity)
		{
			this.SendPropertyChanging();
			entity.user1 = this;
		}
		
		private void detach_cars(car entity)
		{
			this.SendPropertyChanging();
			entity.user1 = null;
		}
	}
}
#pragma warning restore 1591
