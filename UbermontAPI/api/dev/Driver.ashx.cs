﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UbermontAPI.api.dev
{
    /// <summary>
    /// Summary description for Driver
    /// </summary>
    public class Driver : IHttpHandler
    {
        UbermontDBDataContext db = new UbermontDBDataContext();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";
            //context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            //context.Response.Headers["Access-Control-Allow-Headers"] = "Authorization, x-domain-accesskey, X-Requested-With";

            #region API Key
            // If in the DEV directory, don't require a key
            if (!context.Request.Path.Contains("api/dev"))
            {

                // Check to make sure the right key is involved
                // API REQUIREMENTS: ?key=[application_key]&key_name=[application_name]
                if (!string.IsNullOrEmpty(context.Request.Params["key"]))
                {
                    AccessKey key = db.AccessKeys.SingleOrDefault(ak => ak.key == context.Request.Params["key"] && ak.name == context.Request.Params["key_name"]);
                    if (key == null)
                    {
                        context.Response.StatusCode = 401;
                        return;
                    }
                }
                else
                {
                    context.Response.StatusCode = 401;
                    return;
                }
            }

            #endregion

            switch (context.Request.Params["action"])
            {
                case "new_driver":
                    context.Response.Write(NewDriver(context.Request.Params["id"]));
                    return;
                case "set_driver_active":
                    context.Response.Write(SetDriverActive(context.Request.Params["id"], bool.Parse(context.Request.Params["is_active"])));
                    return;
                case "find_active_drivers":
                    context.Response.Write(FindActiveDrivers());
                    return;
                case "is_user_a_driver":
                    context.Response.Write(IsUserADriver(context.Request.Params["id"]));
                    return;
                case "get_driver":
                    context.Response.Write(GetDriver(context.Request.Params["id"]));
                    return;
                case "update_position":
                    context.Response.Write(UpdatePosition(context.Request.Params["id"], context.Request.Params["position"]));
                    return;
                case "start_route":
                    context.Response.Write(StartRoute(context.Request.Params["id"], context.Request.Params["spos"], context.Request.Params["epos"], context.Request.Params["cpos"]));
                    return;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        #region Driver

        string NewDriver(string user_id)
        {
            driver d = new driver()
            {
                user = user_id,
                active = false
            };

            try
            {
                db.drivers.InsertOnSubmit(d);
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }
        }

        string SetDriverActive(string driver_id, bool is_active)
        {

            driver dbDriver = db.drivers.SingleOrDefault(d => d.user == driver_id);

            if (dbDriver == null) return JsonConvert.SerializeObject(new Result(new Exception("Unable to find driver")));

            try
            {
                dbDriver.active = is_active;
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }

        }

        string FindActiveDrivers()
        {
            List<driver> drivers = db.drivers.Where(d => d.active == true).ToList();

            return JsonConvert.SerializeObject(drivers, Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

        }

        string IsUserADriver(string user_id)
        {
            if (string.IsNullOrEmpty(user_id))
                return JsonConvert.SerializeObject(new Result(new Exception("Not all parameters were given.")));

            try
            {
                return JsonConvert.SerializeObject(new Result(rtn: db.drivers.Count() == 1));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex);
            }
        }

        string GetDriver(string user_id)
        {
            if (string.IsNullOrEmpty(user_id))
                return JsonConvert.SerializeObject(new Result(new Exception("Not all parameters were given.")));

            try
            {
                driver dr = db.drivers.SingleOrDefault(d => d.user == user_id);
                if (dr == null)
                    return JsonConvert.SerializeObject(new Result(new Exception("No driver found!")));
                else
                    return JsonConvert.SerializeObject(new Result(dr), Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }
        }

        string StartRoute(string user_id, string start_position, string end_position, string current_position)
        {
            if (string.IsNullOrEmpty(user_id))
                return JsonConvert.SerializeObject(new Result(new Exception("Not all parameters were given.")));

            try
            {
                driver dr = db.drivers.SingleOrDefault(d => d.user == user_id);
                if (dr == null)
                    return JsonConvert.SerializeObject(new Result(new Exception("No driver found!")));

                dr.start_position = start_position;
                dr.end_position = end_position;
                dr.current_position = current_position;
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }
        }

        string UpdatePosition(string user_id, string position)
        {
            if (string.IsNullOrEmpty(user_id))
                return JsonConvert.SerializeObject(new Result(new Exception("Not all parameters were given.")));

            try
            {
                driver dr = db.drivers.SingleOrDefault(d => d.user == user_id);
                if (dr == null)
                    return JsonConvert.SerializeObject(new Result(new Exception("No driver found!")));

                dr.current_position = position;
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }
        }

        string RequestDriver(string username_from, string username_to)
        {
            // For now just send a push
            Notifications.SendToast(username_from + " has requested you!", username_to);

            return JsonConvert.SerializeObject(new Result());
        }

        #endregion
    }
}