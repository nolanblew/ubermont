﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace UbermontAPI.api.dev
{
    /// <summary>
    /// Summary description for User
    /// </summary>
    public class User : IHttpHandler
    {

        UbermontDBDataContext db = new UbermontDBDataContext();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";
            //context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            //context.Response.Headers["Access-Control-Allow-Headers"] = "Authorization, x-domain-accesskey, X-Requested-With";

            #region API Key
            // If in the DEV directory, don't require a key
            if (!context.Request.Path.Contains("api/dev"))
            {

                // Check to make sure the right key is involved
                // API REQUIREMENTS: ?key=[application_key]&key_name=[application_name]
                if (!string.IsNullOrEmpty(context.Request.Params["key"]))
                {
                    AccessKey key = db.AccessKeys.SingleOrDefault(ak => ak.key == context.Request.Params["key"] && ak.name == context.Request.Params["key_name"]);
                    if (key == null)
                    {
                        context.Response.StatusCode = 401;
                        return;
                    }
                }
                else
                {
                    context.Response.StatusCode = 401;
                    return;
                }
            }

            #endregion

            switch (context.Request.Params["action"])
            {
                case "login":
                    context.Response.Write(LoginUser(context.Request.Params["username"], context.Request.Params["password"]));
                    return;
                case "get_user":
                    context.Response.Write(GetUser(context.Request.Params["username"]));
                    return;
                case "register":
                    context.Response.Write(RegisterUser(context.Request.Params["username"], context.Request.Params["password"], context.Request.Params["full_name"]));
                    return;
                case "update_push":
                    context.Response.Write(UpdatePush(context.Request.Params["username"], context.Request.Params["push_channel"]));
                    return;
                case "send_toast":
                    Notifications.SendToast(context.Request.Params["message"]);
                    context.Response.Write("Sent toast notification!");
                    return;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        #region Login

        string LoginUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return JsonConvert.SerializeObject(new NullReferenceException("Not all parameters were given."));

            UbermontAPI.user u = db.users.SingleOrDefault(us => us.username == username.ToLower() && us.password == password);

            if (u != null)
                return JsonConvert.SerializeObject(new { auth = true });
            else
                return JsonConvert.SerializeObject(new { auth = false });
        }

        string GetUser(string username)
        {
            if (string.IsNullOrEmpty(username))
                return JsonConvert.SerializeObject(new NullReferenceException("Not all parameters were given."));

            UbermontAPI.user u = db.users.SingleOrDefault(us => us.username == username.ToLower());

            if (u != null)
            {
                u.password = "";
                return JsonConvert.SerializeObject(u, Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            else
                return JsonConvert.SerializeObject(new UbermontAPI.user(), Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        string RegisterUser(string username, string password, string full_name, int devices = 1)
        {
            Result result = new Result();

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(full_name))
                return JsonConvert.SerializeObject(new Result() { Status = "error", Message = "Not all parameters were given. Please refer to API documentation.", StackTrace = "URL Param Check" });

            // Try to register the user
            if (db.users.Count(u => u.username == username) > 0)
            {
                result.Status = "error";
                result.Message = "User already exists.";
                result.StackTrace = "Check if user exists";
            }
            else
            {
                UbermontAPI.user u = new UbermontAPI.user()
                {
                    username = username,
                    password = password,
                    full_name = full_name,
                    date_created = DateTime.Now,
                    last_accesed = DateTime.Now,
                    devices = devices
                };

                try
                {
                    db.users.InsertOnSubmit(u);
                    db.SubmitChanges();

                    result.Status = "success";
                    result.Message = "";

                }
                catch (Exception ex)
                {
                    result.Status = "error";
                    result.Message = ex.Message;
                    result.Status = ex.StackTrace;
                }
            }

            string json = JsonConvert.SerializeObject(result);
            return json;
        }

        public string UpdatePush(string username, string PushChannel)
        {
            if (string.IsNullOrEmpty(username))
                return JsonConvert.SerializeObject(new NullReferenceException("Not all parameters were given."));

            UbermontAPI.user u = db.users.SingleOrDefault(us => us.username == username.ToLower());

            if (u == null)
                return JsonConvert.SerializeObject(new Result(new Exception("Error: User not found")));

            u.push_channel = PushChannel;

            try
            {
                db.SubmitChanges();
                return JsonConvert.SerializeObject(new Result());

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }


        }

        #endregion
    }


    public class Result
    {
        #region Constructors

        /// <summary>
        /// Creates a new message class with a status
        /// </summary>
        /// <param name="isSuccess">True (default) to show success, false if otherwise</param>
        public Result(bool isSuccess = true)
        {
            if (isSuccess)
                Status = "Success";
            else
                Status = "Error";
        }

        /// <summary>
        /// Creates a new result class with a return object and sets the status to Success
        /// </summary>
        /// <param name="rtn"></param>
        public Result(object rtn)
        {
            Return(rtn);
            Status = "Success";
        }

        /// <summary>
        /// Creates a new result class, with a custom indication
        /// </summary>
        /// <param name="status">The status of the message</param>
        public Result(string status) : this(status, "", "") { }

        /// <summary>
        /// Creates a new result class with a status "Error", and the exception message and stack trace in the result
        /// </summary>
        /// <param name="exception">The exception to pull the message and stacktrace from</param>
        public Result(Exception exception)
        {
            Status = "Error";
            Message = exception.Message;
            StackTrace = exception.StackTrace;
        }

        /// <summary>
        /// Creates a new custom message class
        /// </summary>
        /// <param name="status">The status of the message. Usually "Error" or "Success"</param>
        /// <param name="message">The message to be delivered</param>
        /// <param name="stackTrace">The stacktrace of the result</param>
        public Result(string status, string message, string stackTrace)
        {
            Status = status;
            Message = message;
            StackTrace = stackTrace;
        }

        #endregion

        public string return_encoded;

        public void Return(object rtn)
        {
            return_encoded = JsonConvert.SerializeObject(rtn, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public T Return<T>()
        {
            return JsonConvert.DeserializeObject<T>(return_encoded, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public string Status { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}