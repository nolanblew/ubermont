﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UbermontAPI.api.dev
{
    /// <summary>
    /// Summary description for Car
    /// </summary>
    public class Car : IHttpHandler
    {

        UbermontDBDataContext db = new UbermontDBDataContext();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";
            //context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            //context.Response.Headers["Access-Control-Allow-Headers"] = "Authorization, x-domain-accesskey, X-Requested-With";

            #region API Key
            // If in the DEV directory, don't require a key
            if (!context.Request.Path.Contains("api/dev"))
            {

                // Check to make sure the right key is involved
                // API REQUIREMENTS: ?key=[application_key]&key_name=[application_name]
                if (!string.IsNullOrEmpty(context.Request.Params["key"]))
                {
                    AccessKey key = db.AccessKeys.SingleOrDefault(ak => ak.key == context.Request.Params["key"] && ak.name == context.Request.Params["key_name"]);
                    if (key == null)
                    {
                        context.Response.StatusCode = 401;
                        return;
                    }
                }
                else
                {
                    context.Response.StatusCode = 401;
                    return;
                }
            }

            #endregion

            switch (context.Request.Params["action"])
            {
                case "update_car":
                    context.Response.Write(UpdateCar(context.Request.Params["username"], context.Request.Params["make"], context.Request.Params["model"], context.Request.Params["year"], context.Request.Params["photo"]));
                    return;
                case "set_car_active":
                    context.Response.Write(SetCarActive(context.Request.Params["username"], bool.Parse(context.Request.Params["is_active"])));
                    return;
                case "get_drivers_car":
                    context.Response.Write(GetDriversCar(context.Request.Params["username"]));
                    return;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        #region Car

        string UpdateCar(string username, string make, string model, string year, string photo)
        {

            car c = db.cars.SingleOrDefault(car => car.user == username);

            if (c == null)
            {

                c = new car()
                {
                    user = username,
                    active = true,
                    manufacturer = make,
                    model = model,
                    year = year,
                    photo = photo
                };

                db.cars.InsertOnSubmit(c);
            } else
            {
                c.manufacturer = make;
                c.model = model;
                c.year = year;
                c.photo = photo;
            }

            try
            {
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }
        }

        string SetCarActive(string user_id, bool is_active)
        {

            car dbCar = db.cars.SingleOrDefault(c => c.user == user_id);

            if (dbCar == null) return JsonConvert.SerializeObject(new Result(new Exception("Unable to find car")));

            try
            {
                dbCar.active = is_active;
                db.SubmitChanges();

                return JsonConvert.SerializeObject(new Result());

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new Result(ex));
            }

        }

        string GetDriversCar(string username)
        {
            car c = db.cars.SingleOrDefault(car => car.user == username && car.active == true);

            if (c == null) return JsonConvert.SerializeObject(new Result(new Exception("Unable to find car")));

            return JsonConvert.SerializeObject(new Result(c), Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        #endregion
    }
}