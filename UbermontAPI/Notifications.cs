﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Notifications;

namespace UbermontAPI
{
    public static class Notifications
    {

        static NotificationHubClient hub = NotificationHubClient.CreateClientFromConnectionString("Endpoint=sb://ubermonthub-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=BBqRuSvstv+ea6EY44exO1vjm6Uvmau6TkwhYcGkxS0=", "ubermonthub");


        public static async Task SendToast(string message, string username = "")
        {
            var toast = string.Format(@"<toast><visual><binding template=""ToastText01""><text id=""1"">{0}</text></binding></visual></toast>", message);

            if (username != "")
            {
                var tags = new List<string>();
                tags.Add(username);
                await hub.SendMpnsNativeNotificationAsync(toast, tags);
            }
            else
            {
                await hub.SendWindowsNativeNotificationAsync(toast);
            }

        }

    }
}
