var Ubermont;
(function (Ubermont) {
    function getList() {
        document.getElementById('list').innerHTML = "<div class='list-group'>" + generateListItem("James Smith", 4) + generateListItem("Emily Ragen", 7) + "</div>";
    }
    Ubermont.getList = getList;
    function generateListItem(name, time) {
        var rtn = "<a href='driver-view.html' class='list-group-item'><h4 class='list-group-item-heading'>" + name + "</h4><p class='list-group-item-text'>" + time + " minutes away</p></a>";
        return rtn;
    }
})(Ubermont || (Ubermont = {}));
//# sourceMappingURL=driver-list.js.map