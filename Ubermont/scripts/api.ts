﻿/// <reference path="typings/jquery.d.ts" />

module Ubermont {

    export module API {

        var APIURL = "http://ubermont-api.azurewebsites.net/api/dev/";
        var api_key = "+suFW7ZbMgVRZgqTzwvcg4XSrzDvRA5u3hgErCmTeUURrmNx8LvmXvEdUSkbvq6D2sW85HpE9yceuTcw452UZM29wgX4mjcDkCn6ub5hhRnbF2mMWbSCuJc5w==";
        var api_name = "UbermontOfficial";

        export function login(username: string, password: string, success) {

            $.ajax({
                url: APIURL + "User.ashx",
                dataType: 'json',
                data: {
                    key: api_key,
                    key_name: api_name,
                    action: 'login',
                    username: username,
                    password: password
                },
                success: function(data) {
                    success && success(data.auth);
                },
                error: function(data, errorStatus, errorThrown) {
                    alert("Error! " + errorStatus + ". " + errorThrown);
                }
            });
        }

    }

}