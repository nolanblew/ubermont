﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
module Ubermont {
    "use strict";

    export module Application {

        export function initialize() {
            document.addEventListener('deviceready', onDeviceReady, false);
        }

        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause, false);
            document.addEventListener('resume', onResume, false);

            // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        }

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        }

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        }

        function pageLoad() {
            document.getElementById("navbar").innerHTML = "<nav class='navbar navbar-default'><div class='container-fluid'><div class='navbar-header'><button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar-collapse-1'><span class='sr-only'>Toggle navigation</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span></button><a class='navbar-brand' href='#'>Ubermont</a></div><div class='collapse navbar-collapse' id='navbar-collapse-1'><ul class='nav navbar-nav navbar-right'><li><a href='#'>Find a Ride</a></li><li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='true'>Groups <span class='caret'></span></a><ul class='dropdown-menu' role='menu'><li><a href='#'>My Group</a></li><li class='divider'></li><li><a href='#'>Manage Groups</a></li></ul></li><li class='divider'></li><li><a href='#'>Switch to Driver Mode</a></li><li><a href='#'>Settings</a></li></ul></div></div></nav>";
        }

    }

    window.onload = function () {
        Application.initialize();
    }

    export function Login_Click() {
        document.location.href = "login.html";
    }

}
