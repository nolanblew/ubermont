﻿/// <reference path="api.ts"/>

module Ubermont {

    export function tryLogin() {

        var username = $("#username").val();
        var password = $("#password").val();

        Ubermont.API.login(username, password, function (result) {
            if (result) {
                window.location.href = "pages/driver-list.html";
            } else {
                $("#errorBox").html("<div class='alert alert-danger'>Incorrect username or password. Please try again.</div>");
                $("#password").val("");
            }
        });

    }

}