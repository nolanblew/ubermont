﻿module Ubermont {

    export function getList() {

        document.getElementById('list').innerHTML = "<div class='list-group'>" + generateListItem("James Smith", 4) + generateListItem("Emily Ragen", 7) + "</div>";

    }

    function generateListItem(name: string, time: number) {

        var rtn: string = "<a href='driver-view.html' class='list-group-item'><h4 class='list-group-item-heading'>" + name + "</h4><p class='list-group-item-text'>" + time + " minutes away</p></a>";
        return rtn;

    }

    //export function driverListOnLoad() {

    //    // Check the battery life
    //    window.addEventListener("batterycritical", onBatteryCritical, false);

    //    function onBatteryCritical(info) {
    //        if (info.isPlugged == true) {
    //            alert("Please keep your device plugged in while using this app so it doesn't die!");
    //        } else {
    //            alert("Your battery is low! Please plug in your device to avoid killing the battery!");
    //        }
    //    }
    //}
    

}